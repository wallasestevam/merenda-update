package wallas.initialization;

import java.awt.Dimension;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import wallas.util.process.RuntimeProcess;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class Inicio {

    public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException {
        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        initJFrameApplication();
    }

    private static void initJFrameApplication() {
        RuntimeProcess comando = new RuntimeProcess();
        comando.execute("java -jar " + "Merenda.jar");

        if (comando.isErrorRunning()) {
            JTextArea jta = new JTextArea(comando.getErroMessage());
            JScrollPane jsp = new JScrollPane(jta) {
                @Override
                public Dimension getPreferredSize() {
                    return new Dimension(480, 320);
                }
            };
            JOptionPane.showMessageDialog(null, jsp, "Erro na execução do programa", JOptionPane.ERROR_MESSAGE);
        }
    }

}
